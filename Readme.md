Набор скриптов для Ansible, которые разворачивают Moodle на виртуальной машине.

В данном примере используется VirtualBox, управляемый Vagrant, с гостевой ОС Ubuntu 16.04 LTS

В group_vars должны быть определены следующие переменные:

 - moodle_data_folder
 - postgres_user
 - postgres_password
 - postgres_db
 - ubuntu_home_folder
 - ubuntu_user
 - vagrant_ip
 - vagrant_folder
